# FFT Transcriber

É um software para transcrições de áudio que, além de focar na usabilidade do
processo de transcrição manual, também possui uma opção para transcrição através
de reconhecimento automático de voz. A aplicação foi desenvolvida em C++ em
conjunto com a API WxWidgets e utiliza, para reconhecimento, o decodificador
Julius que é interfaceado através do software Coruja, uma API disponível neste
site para facilitar a programação de aplicativos utilizando reconhecimento de
voz

Manual: uma descrição mais detalhada das funcionalidades do aplicativo pode ser
encontrada na sua documentação em PDF.
